import React from 'react';
import { slide as Menu } from 'react-burger-menu'; 
import './sidebar.css';

export default props => {
  return (
    <Menu>
      <a className="menu-item" href="/HomePage">
        HomePage
      </a>

      <a className="menu-item" href="/About">
        About
      </a>

      <a className="menu-item" href="/Cources">
        Cources
      </a>

      <a className="menu-item" href="/Gallery">
        Gallery
      </a>

      <a className="menu-item" href="/Contacts">
        Contacts
      </a>
    </Menu>
  );
};

