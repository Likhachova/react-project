import React from "react";
import Footer from "../Footer/Footer";
import Table from "../Table/Table";
import CustomInput from "../FormItems/CustomInput";
import CoursesSelect from "../FormItems/CoursesSelect";
import Sidebar from "../Sidebar/Sidebar";

const About = () => (
<div class="about-section">
<Sidebar/>
<h1>About</h1>
<Table/>
<h1>Fill in the form</h1>
<CustomInput/>
<CoursesSelect/>
<Footer/>
</div>
);

export default About;