import React from "react";
import Footer from "../Footer/Footer";
import CardAngular from "../Card/CardAngular";
import CardJavascript from "../Card/CardJavascript";
import CardJava from "../Card/CardJava";
import CardReactjs from "../Card/CardReactjs";
import CardNodeJs from "../Card/CardNodeJs";
import CardVueJs from "../Card/CardVueJs";
import Sidebar from "../Sidebar/Sidebar";

const Courses = () => (  
<div class="courses-body">
<Sidebar/>
<h1 class="courses-names">Courses</h1>
<div class="courses-list">
<div class="courses-item"><CardReactjs/></div>
<div class="courses-item"><CardAngular/></div>
<div class="courses-item"><CardJava/></div>
<div class="courses-item"><CardJavascript/></div>
<div class="courses-item"><CardNodeJs/></div>
<div class="courses-item"><CardVueJs/></div>
</div>
<Footer/>
</div>);

export default Courses;