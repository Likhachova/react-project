import React from "react";
import Footer from "../Footer/Footer";
import Loader from "../Loader/Loader";
import CommentTextArea from "../FormItems/CommentTextArea";
import Sidebar from "../Sidebar/Sidebar";

const Contacts = () => (
<div>
<Sidebar/>
<h1> Our contacts</h1>
<div>
Youth 4 Innovation (170 Columna St.).</div>
<img src="/static/images/building.jpg" alt="venue"/>
<CommentTextArea/>
<Loader/>
<Footer/>
</div>
);

export default Contacts;