import React from 'react'
import Footer from "../Footer/Footer";
import Pagination from "../Pagination/PaginationCustom";
import Sidebar from "../Sidebar/Sidebar";
import Button from "../Buttons/Button"

const Gallery = () =>  (
            <div class="gallery-section">
            <Sidebar/>
            <h1>Gallery</h1>
            <Pagination/>
            <Button/>
            <Footer/>
            </div>
            );

export default Gallery;