import React from "react";
import Footer from "../Footer/Footer";
import HomePageTabs from "../Tabs/HomePageTabs";
import Sidebar from "../Sidebar/Sidebar";

const HomePage = () => (
<div>
<Sidebar/>
<h1>HomePage</h1>
<HomePageTabs/>
<Footer/>
</div>
);

export default HomePage;