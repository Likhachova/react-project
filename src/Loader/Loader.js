import React, { Component }  from 'react';
import { BallBeat } from 'react-pure-loaders';
import './loader.css';

class Loader extends Component {
  constructor(props) {
    super(props);
    this.buttonPressed = this.buttonPressed.bind(this);
    this.state = {
      loading: false,
    };
  }

  buttonPressed(){
    this.setState({ loading: !this.state.loading });
  };

  render() {
    return (
      <div>
        <BallBeat
          color={'#123abc'}
          loading={this.state.loading}
        />
        <div  class="loader-button" onClick={this.buttonPressed} ><p>Download</p></div>
      </div>
    )
  }

   
}

export default Loader