import { useState } from 'react'

const UseBreadcrumb = () => {
  const [expanded, setExpanded] = useState(false)

  const open = () => setExpanded(true)

  return {
    expanded,
    open,
  }
}

export default UseBreadcrumb