import React from 'react'
import { NavLink } from 'react-router-dom';
import Menu from '../Menu';
import './header.css';

const Navigation = () => (
    <nav className="header">
        {
            Menu.map( (route, key) => {
                if( route.path && !route.dontShowInMenu){
                    return(
                        <NavLink key={key} to={route.path}>
                            { route.title}
                        </NavLink>
                    )
                }
                
            })
        }
    </nav>
);

export default Navigation;