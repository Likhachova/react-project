import React from 'react';

const ButtonCustom = ({action, style, textValue, children}) => (
  <div>
  <button style={style} onClick={action}>
    {
      children === undefined ?
        textValue : 
        children
    }
  </button>
  </div>
);

ButtonCustom.defaultProps = {
  style: {
    padding: '5px 20px',
    width: '200px',
    height: '50px',
    margin: '40px 110px',
    color: 'palevioletred',
    fontSize: '20px',
    backgroundColor: 'papayawhip'
  },
  action: () => {
    window.location.assign('https://ru.reactjs.org/');
  },
  textValue: "Learn React"
}

export default ButtonCustom;
