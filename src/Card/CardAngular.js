import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: '40px auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', 
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  function handleExpandClick() {
    setExpanded(!expanded);
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            A
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Angular"
        subheader="TypeScript-based framework"
      />
      <CardMedia
        className={classes.media}
        image="/static/logos/angular.png"
        title="Angular"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        Angular is a TypeScript-based open-source web application framework led by the Angular 
        Team at Google and by a community of individuals and corporations. Angular is a 
        complete rewrite from the same team that built AngularJS.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Description:</Typography>
          <Typography paragraph>
          Angular was ranked the second most used technology after Node.js in the 2018 Stack Overflow survey. 
          The reasons for such a high rank are clear: this frontend tool is equipped with robust components 
          to help developers write readable, maintainable, and easy-to-use code. But despite all the strengths 
          of Angular, you should understand that sometimes this tool can work against you and your service rather than help it.  
          </Typography>
          <Typography paragraph>
          Angular (also referred to as Angular 2+) is a web development platform built in TypeScript 
          that provides developers with robust tools for creating the client side of web applications.  
          Released in 2010 and formerly known as AngularJS, Angular is a JavaScript framework that was 
          initially geared toward building single-page applications. At the time, the popularity of 
          SPAs was growing rapidly, and so did the popularity of the AngularJS framework. But over 
          time, the emergence of new tools like React and Vue, which offered wider functionality, 
          made the Angular team think about improving their product.
          </Typography>
          <Typography paragraph>
          Why Angular?
          So, how developers can benefit from using Angular in their projects? Here we’ll focus on Angular 2+, 
          omitting AngularJS since we don’t use the first version in our projects. The strong sides of Angular include:

          Detailed documentation. Angular boasts detailed documentation where developers can find all necessary 
          information without asking their colleagues. As a result, developers can quickly come up with technical 
          solutions and resolve emerging issues. 

          Support by Google. A lot of developers consider Google support another benefit of Angular, making the 
          platform trustworthy. At ng-conf 2017, the developers of Angular confirmed that Google will support Angular 
          on a long-term basis.

          Great ecosystem of third-party components. The popularity of Angular has resulted in the appearance of thousands 
          of additional tools and components that can be used in Angular apps. As a result, you can get additional 
          functionality and productivity improvements.

          Component-based architecture. In the second version, Angular shifted from an MVC to a component-based architecture. 
          According to this architecture, an app is divided into independent logical and functional components. 
          These components can easily be replaced and decoupled as well as reused in other parts of an app. In addition, 
          component independence makes it easy to test a web app and ensure that every component works seamlessly.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

