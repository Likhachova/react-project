import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: '40px auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', 
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  function handleExpandClick() {
    setExpanded(!expanded);
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="language" className={classes.avatar}>
            N
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Node.js"
        subheader="Cross-platform runtime environment"
      />
      <CardMedia
        className={classes.media}
        image="/static/logos/nodejs.png"
        title="Node.js"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        Node.js is an open source, cross-platform runtime environment for developing server-side 
        and networking applications. Node.js applications are written in JavaScript, and can be 
        run within the Node.js runtime on OS X, Microsoft Windows, and Linux.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Description:</Typography>
          <Typography paragraph>
          Where to Use Node.js?
          Following are the areas where Node.js is proving itself as a perfect technology partner.
          I/O bound Applications
          Data Streaming Applications
          Data Intensive Real-time Applications (DIRT)
          JSON APIs based Applications
          Single Page Applications
          </Typography>
          <Typography paragraph>
          Who Uses Node.js?
          Following is the link on github wiki containing an exhaustive list of projects, application 
          and companies which are using Node.js. This list includes eBay, General Electric, GoDaddy, 
          Microsoft, PayPal, Uber, Wikipins, Yahoo!, and Yammer to name a few.
          </Typography>
          <Typography paragraph>
          Features of Node.js
          Following are some of the important features that make Node.js the first choice of software architects.
          Asynchronous and Event Driven − All APIs of Node.js library are asynchronous, that is, non-blocking. 
          It essentially means a Node.js based server never waits for an API to return data. The server moves 
          to the next API after calling it and a notification mechanism of Events of Node.js helps the server 
          to get a response from the previous API call.
          Very Fast − Being built on Google Chrome's V8 JavaScript Engine, Node.js library is very fast in code execution.
          Single Threaded but Highly Scalable − Node.js uses a single threaded model with event looping. 
          Event mechanism helps the server to respond in a non-blocking way and makes the server highly scalable 
          as opposed to traditional servers which create limited threads to handle requests. Node.js uses a single 
          threaded program and the same program can provide service to a much larger number of requests than 
          traditional servers like Apache HTTP Server.
          No Buffering − Node.js applications never buffer any data. These applications simply output the data in chunks.
          License − Node.js is released under the MIT license.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

