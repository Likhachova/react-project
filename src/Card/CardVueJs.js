import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: '40px auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', 
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  function handleExpandClick() {
    setExpanded(!expanded);
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="language" className={classes.avatar}>
            V
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Vue.js"
        subheader="JavaScript framework"
      />
      <CardMedia
        className={classes.media}
        image="/static/logos/vuejs.png"
        title="Vue.js"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Description:</Typography>
          <Typography paragraph>
          Vue uses an HTML-based template syntax that allows binding the rendered DOM to the underlying 
          Vue instance's data. All Vue templates are valid HTML that can be parsed by specification-compliant 
          browsers and HTML parsers.
          </Typography>
          <Typography paragraph>
          Vue compiles the templates into virtual DOM render functions. A virtual 
          Document Object Model (or “DOM”) allows Vue to render components in its memory before updating the browser. 
          Combined with the reactivity system, Vue is able to calculate the minimal number of components 
          to re-render and apply the minimal amount of DOM manipulations when the app state changes.
          </Typography>
          <Typography paragraph>
          Vue features a reactivity system that uses plain JavaScript objects and optimized re-rendering. 
          Each component keeps track of its reactive dependencies during its render, so the system knows 
          precisely when to re-render, and which components to re-render.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

