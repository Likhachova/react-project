import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: '40px auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', 
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  function handleExpandClick() {
    setExpanded(!expanded);
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="language" className={classes.avatar}>
            J
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Java"
        subheader="General-purpose programming language"
      />
      <CardMedia
        className={classes.media}
        image="/static/logos/java.png"
        title="Java"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        Java is a general-purpose programming language that is class-based, object-oriented, 
          and designed to have as few implementation dependencies as possible. 
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Description:</Typography>
          <Typography paragraph>
          It is intended to let application developers write once, run anywhere (WORA), meaning that compiled 
          Java code can run on all platforms that support Java without the need for recompilation.
          Java applications are typically compiled to bytecode that can run on any Java virtual machine 
          (JVM) regardless of the underlying computer architecture.
          </Typography>
          <Typography paragraph>
          The latest versions are Java 13, released in September 2019, and Java 11, a currently supported 
          long-term support (LTS) version, released on September 25, 2018; Oracle released for the legacy 
          Java 8 LTS the last free public update in January 2019 for commercial use, while it will otherwise 
          still support Java 8 with public updates for personal use up to at least December 2020. Oracle (and others) 
          highly recommend uninstalling older versions of Java because of serious risks due to unresolved security issues.
          Since Java 9 (and 10 and 12) is no longer supported, Oracle advises its users to immediately transition to Java 
          11 (Java 13 is also a non-LTS option).
          </Typography>
          <Typography paragraph>
          There were five primary goals in the creation of the Java language:
          It must be simple, object-oriented, and familiar.
          It must be robust and secure.
          It must be architecture-neutral and portable.
          It must execute with high performance.
          It must be interpreted, threaded, and dynamic.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

