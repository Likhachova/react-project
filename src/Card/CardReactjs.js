import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: '40px auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', 
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  function handleExpandClick() {
    setExpanded(!expanded);
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="language" className={classes.avatar}>
            R
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="React"
        subheader="JavaScript library"
      />
      <CardMedia
        className={classes.media}
        image="/static/logos/react.png"
        title="React"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        React is a JavaScript library for building user interfaces. 
        React makes building and maintaining the user interface of your application 
        faster and easier by breaking it up into smaller, reusable components. 
        It also helps eliminate the complexity that comes with updating your DOM 
        elements when the user interacts with your application.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Description:</Typography>
          <Typography paragraph>
          React (also known as React.js or ReactJS) is a JavaScript library
          for building user interfaces. It is maintained by Facebook and a community of individual developers and companies
          </Typography>
          <Typography paragraph>
          React can be used as a base in the development of single-page or mobile applications, as it is optimal for 
          fetching rapidly changing data that needs to be recorded. However, fetching data is only the beginning of 
          what happens on a web page, which is why complex React applications usually require the use of additional 
          libraries for state management, routing, and interaction with an API: Redux, React Router and 
          axios are examples of such libraries.
          </Typography>
          <Typography paragraph>
          Another notable feature is the use of a virtual Document Object Model, or virtual DOM. React creates an 
          in-memory data-structure cache, computes the resulting differences, and then updates the browser's displayed 
          DOM efficiently. This allows the programmer to write code as if the entire page is rendered on each change, 
          while the React libraries only render subcomponents that actually change.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

