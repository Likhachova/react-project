import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    margin: '40px auto',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', 
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  function handleExpandClick() {
    setExpanded(!expanded);
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="language" className={classes.avatar}>
            JS
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="JavaScript"
        subheader="Scripting or programming language"
      />
      <CardMedia
        className={classes.media}
        image="/static/logos/js.png"
        title="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        JavaScript is a scripting or programming language that allows you to implement 
          complex things on web pages.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Description:</Typography>
          <Typography paragraph>
          Every time a web page does more than just sit 
          there and display static information for you to look at — displaying timely 
          content updates, interactive maps, animated 2D/3D graphics, scrolling video 
          jukeboxes, etc. — you can bet that JavaScript is probably involved. 
          It is the third layer of the layer cake of standard web technologies, 
          two of which (HTML and CSS) we have covered in much more detail in other 
          parts of the Learning Area.
          </Typography>
          <Typography paragraph>
          The core client-side JavaScript language consists of some common programming features that allow you to do things like:
          Store useful values inside variables. In the above example for instance, we ask for a new name to be entered then 
          store that name in a variable called name.
          Operations on pieces of text (known as "strings" in programming). In the above example we take the string 
          "Player 1: " and join it to the name variable to create the complete text label, e.g. ''Player 1: Chris".
          Running code in response to certain events occurring on a web page. We used a click event in our example 
          above to detect when the button is clicked and then run the code that updates the text label.
          </Typography>
          <Typography paragraph>
          Browser APIs are built into your web browser, and are able to expose data from the surrounding computer environment, 
          or do useful complex things. For example:
          The DOM (Document Object Model) API allows you to manipulate HTML and CSS, creating, removing and changing HTML, 
          dynamically applying new styles to your page, etc. Every time you see a popup window appear on a page, or some new 
          content displayed (as we saw above in our simple demo) for example, that's the DOM in action.
          The Geolocation API retrieves geographical information. This is how Google Maps is able to find your location and 
          plot it on a map.
          The Canvas and WebGL APIs allow you to create animated 2D and 3D graphics. People are doing some amazing things using 
          these web technologies —see Chrome Experiments and webglsamples.
          Audio and Video APIs like HTMLMediaElement and WebRTC allow you to do really interesting things with multimedia, 
          such as play audio and video right in a web page, or grab video from your web camera and display it on someone 
          else's computer (try our simple Snapshot demo to get the idea).
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

