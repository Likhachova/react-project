import HomePage from './pages/HomePage';
import About from './pages/About';
import Contacts from './pages/Contacts';
import Courses from './pages/Courses';
import Gallery from './pages/Gallery';


const Menu = [
    {
        title: 'HomePage',
        path: '/',
        component: HomePage,
        exact: true
    },
    {
        title: 'About',
        path: '/about',
        component: About
    },
    {
        title: 'Contacts',
        path: '/contacts',
        component: Contacts,
    },
    {
        title: 'Courses',
        path: '/courses',
        component: Courses,
    },
    {
        title: 'Gallery',
        path: '/gallery',
        component: Gallery,
    }

]

export default Menu;