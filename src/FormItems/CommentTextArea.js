import React from 'react';
import './formitems.css';

class CommentTextArea extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        value: 'Please write us'
      };
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      event.preventDefault();
    }
  
    render() {
      return (
        <div className="write-us">
        <h1 class="comment-title">Please write us</h1>
          <form onSubmit={this.handleSubmit}>
              <textarea class="comment-textarea" value={this.state.value} onChange={this.handleChange} cols={40} rows={10} />
            <input type="submit" value="Submit" />
          </form>
        </div>
      );
    }
  }
  
  export default CommentTextArea;