import React from 'react';

class CustomInput extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        username: '',
        surname: null,
      };
    }
    myChangeHandler = (event) => {
      let nam = event.target.name;
      let val = event.target.value;
      this.setState({[nam]: val});
    }
    render() {
      return (
        <form>
        <p>Enter your name:</p>
        <input
          type='text'
          name='username'
          onChange={this.myChangeHandler}
        />
        <p>Enter your surname:</p>
        <input
          type='text'
          name='surname'
          onChange={this.myChangeHandler}
        />
        </form>
      );
    }
  }
  
  export default CustomInput;