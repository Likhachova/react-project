import React from 'react';
class CustomSelect extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        property: 'name'
      };
    }
    render() {
      return (
        <form>
          <h1>This is custom select</h1>
        <select value={this.state.property}>
          <option value="name">name</option>
          <option value="surname">surname</option>
          <option value="age">age</option>
        </select>
        </form>
      );
    }
  }
  
  export default CustomSelect;