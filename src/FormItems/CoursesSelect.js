import React from 'react';
import './formitems.css';

const options = [
  { value: 'Angular', label: 'Angular' },
  { value: 'React', label: 'React' },
  { value: 'Javascript', label: 'Javascript' },
  { value: 'Java', label: 'Java' },
  { value: 'VueJS', label: 'VueJS' },
  { value: 'NodeJs', label: 'NodeJs' },
];

class CoursesSelect extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        course: null
      };
    }
    
    state = {
      selectedOption: null,
    };

    handleChange = selectedOption => {
      this.setState({ selectedOption });
    };

    render() {
      const { selectedOption } = this.state;
      return (
        <CoursesSelect
        value={selectedOption}
        onChange={this.handleChange}
        options={options}
      />
      );
    }
  }
  
  export default CoursesSelect;