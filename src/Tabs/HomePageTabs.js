import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      <Box p={4}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
  },
}));

export default function FullWidthTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  function handleChangeIndex(index) {
    setValue(index);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="What you will learn?" {...a11yProps(0)} />
          <Tab label="Who can apply?" {...a11yProps(1)} />
          <Tab label="What is the venue for classes?" {...a11yProps(2)} />
          <Tab label="How to apply?" {...a11yProps(3)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <div>
          The main idea of the project is to show that IT is not as complicated as one may think and gives you a world-class knowledge on the most popular programming languages. The graduates of the program will have strong basis to continue their studies and get an internship in IT company. The course covers main aspects following programming languages: C, Python, JavaScript, HTML/CSS.</div>
          <img src="/static/images/web.jpg" alt="what-learn"/>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <div>
          Participation in the program does not require prior technical knowledge and skills (although it is an advantage). However, the candidate should have upper-intermediate English-language proficiency (since most of the materials are in English).</div>
          <img src="/static/images/suits.jpg" alt="who-apply"/>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <div>
          The classes will be hosted by our partner — Youth 4 Innovation (170 Columna St.).</div>
          <img src="/static/images/building.jpg" alt="venue"/>
        </TabPanel>
        <TabPanel value={value} index={3} dir={theme.direction}>
          <div>
          Selection of students is done in 2 stages: a questionnaire and a test task. On the questionnaire stage candidates with intermediate or higher level of English are chosen. During the test task stage those who perform the task better get privileged.</div>
          <img src="/static/images/start-up.jpg" alt="how-to-apply"/>
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}