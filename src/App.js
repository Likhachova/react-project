import React from 'react';
import './App.css';
import Navigation from './Header/Navigation';
import { Router, Link } from '@reach/router'
import {
  Switch,
  Route
} from 'react-router-dom';
import Menu from './Menu';
import BreadCrumb from './BreadCrumb/BreadCrumb'
import HomePage from './pages/HomePage'
import About from './pages/About'
import Gallery from './pages/Gallery'
import Courses from './pages/Courses'
import Contacts from './pages/Contacts'


const items = [
  { to: '/', label: 'HomePage' },
  { to: '/courses', label: 'Courses' },
  { to: '/gallery', label: 'Gallery' },
  { to: '/about', label: 'About' },
  { to: '/contacts', label: 'Contacts' },
]

function App(props) {
  return (
    <>
    <BreadCrumb separator='/'>
      {items.map(({ to, label }) => (
        <Link key={to} to={to}>
          {label}
        </Link>
      ))}
    </BreadCrumb>
    {/* <Router>
      <HomePage path='/' />
      <Courses path='/courses' />
      <Gallery path='/gallery' />
      <About path='/about' />
      <Contacts path='/contacts' />
    </Router> */}
      <Navigation />
      <Switch key={props.location.key}>
          {
            Menu.map( (route, key) => {
              return (
                <Route key={key} {...route} />
              )
            })
          }
      </Switch>
    </>
  );
}


export default App;
