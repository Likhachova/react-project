import React from "react";
import './footer.css';

function Footer() {
  componentDidMount ();
  return (
    <div class="footer">
     <div class="footer-line"></div>
      <div class="footer-tools">
      <div class="footer-tool"><div class="footer-title">Find us in facebook</div><img src="/static/icons/facebook.png" alt="" class="footer-image"/></div>
      <div class="footer-tool"><div class="footer-title">Find us in twitter</div><img src="/static/icons/twitter.png" alt="" class="footer-image"/></div>
      <div class="footer-tool"><div class="footer-title">Find us in skype</div><img src="/static/icons/skype.png" alt="" class="footer-image"/></div>
      </div>
      <div class="footer-line"></div>
    </div>
  );
}

function componentDidMount () {
  const script = document.createElement("script");
  script.src = "https://use.fontawesome.com/9661f9e0b8.js";
  script.async = true;
  document.body.appendChild(script);
}

export default Footer;